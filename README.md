# ePoXy #



### What is ePoXy? ###

**ePoXy** is the “glue” or social network that holds the **STX** community together 
with **Proof of Stacking** login and authentication.

**ePoXy** is a social network for Stackers that contains the following components:

* Login and authentication via **Proof of Stacking** 

* **RESIN (Request Enabled Stacker Individual Network)** contains a Stacker's profile [description and info] which is searchable by tags and accessible by friend request 

* **Resinate**, a fundraiser for **STX** projects

* **ePoXy wallet** for **STX**

* **Talus** is a tool that automates mining and stacking **STX** from **BTC** earnings